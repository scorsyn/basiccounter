﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BasiCounterLibrary1
{
    public class BasicCounter
    {
        private int total;

        //constructeur
        //IN : valeur initiale du compteur
        //OUT : objet compteur
        public BasicCounter(int ptotal)
        {
            this.total = ptotal;
        }

        //fonction decrement
        //Fonction qui décrémente le compteur
        //IN : rien
        //OUT : rien
        public void decrement()
        {
            if (this.total>0) this.total = this.total - 1;
        }

        //fonction increment
        //Fonction qui incrémente le compteur
        //IN : rien
        //OUT : rien
        public void increment()
        {
            this.total = this.total + 1;
        }

        //fonction raz
        //Fonction qui remet le compteur à 0
        //IN : rien
        //OUT : rien
        public void raz()
        {
            this.total = 0;
        }

        //fonction getTotal
        //Fonction qui récupère la valeur du compteur
        //IN : rien
        //OUT : entier contenant la valeur du compteur
        public int getTotal()
        {
            return this.total;
        }

    }
}
