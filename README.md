﻿# BasicCounter

##TP noté BasicCounter 25 avril 2019

Réalisé dans des conditions compliquées car sur Windows

##UML

--------------------------------------
(C) Basic Counter
--------------------------------------
(Red empty square) total : integer
--------------------------------------
(Green full circle) getTotal() : void
(Green full circle) increment() : void
(Green full circle) decrement() : void
(Green full circle) raz() : void
--------------------------------------

