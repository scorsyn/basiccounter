﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BasiCounterLibrary1;



namespace UnitTestBasicCounter
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestConstructeur()
        {
            BasicCounter test_compteur = new BasicCounter(0);
            Assert.AreEqual(0, test_compteur.getTotal());
        }

        [TestMethod]
        public void TestIncrement()
        {
            BasicCounter test_compteur = new BasicCounter(0);
            test_compteur.increment();
            Assert.AreEqual(1, test_compteur.getTotal());
        }

        [TestMethod]
        public void TestDecrement()
        {
            BasicCounter test_compteur = new BasicCounter(2);
            test_compteur.decrement();
            Assert.AreEqual(1, test_compteur.getTotal());
        }

        [TestMethod]
        public void TestRAZ()
        {
            BasicCounter test_compteur = new BasicCounter(2);
            test_compteur.raz();
            Assert.AreEqual(0, test_compteur.getTotal());
        }
    }
}
