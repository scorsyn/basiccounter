﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using BasiCounterLibrary1;

namespace BasicCounterApp
{
    public partial class Form1 : Form
    {
        //instanciation d'un objet compteur
        BasicCounter compteur = new BasicCounter(0);
        public Form1()
        {
            InitializeComponent();
            //affichage dans la fenêtre de la valeur initiale du compteur
            labelValCompteur.Text = String.Format("{0}", compteur.getTotal());

        }

        //appel de la fonction raz lors d'un clic sur le bouton sur RAZ
        private void razButton_Click(object sender, EventArgs e)
        {
            compteur.raz();
            labelValCompteur.Text = String.Format("{0}",compteur.getTotal());
        }

        //appel de la fonction decrement lors d'un clic sur le bouton sur -
        private void decButton_Click(object sender, EventArgs e)
        {
            compteur.decrement();
            labelValCompteur.Text = String.Format("{0}",compteur.getTotal());
        }

        //appel de la fonction increment lors d'un clic sur le bouton sur +
        private void incButton_Click(object sender, EventArgs e)
        {
            compteur.increment();
            labelValCompteur.Text = String.Format("{0}",compteur.getTotal());
        }
    }
}
