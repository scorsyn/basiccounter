﻿namespace BasicCounterApp
{
    partial class Form1
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.incButton = new System.Windows.Forms.Button();
            this.decButton = new System.Windows.Forms.Button();
            this.razButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.labelValCompteur = new System.Windows.Forms.Label();
            this.title = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // incButton
            // 
            this.incButton.BackColor = System.Drawing.Color.DarkGreen;
            this.incButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.incButton.ForeColor = System.Drawing.Color.White;
            this.incButton.Location = new System.Drawing.Point(321, 73);
            this.incButton.Name = "incButton";
            this.incButton.Size = new System.Drawing.Size(103, 63);
            this.incButton.TabIndex = 0;
            this.incButton.Text = "+";
            this.incButton.UseVisualStyleBackColor = false;
            this.incButton.Click += new System.EventHandler(this.incButton_Click);
            // 
            // decButton
            // 
            this.decButton.BackColor = System.Drawing.Color.DarkRed;
            this.decButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.decButton.ForeColor = System.Drawing.Color.White;
            this.decButton.Location = new System.Drawing.Point(12, 73);
            this.decButton.Name = "decButton";
            this.decButton.Size = new System.Drawing.Size(103, 63);
            this.decButton.TabIndex = 1;
            this.decButton.Text = "-";
            this.decButton.UseVisualStyleBackColor = false;
            this.decButton.Click += new System.EventHandler(this.decButton_Click);
            // 
            // razButton
            // 
            this.razButton.Location = new System.Drawing.Point(149, 157);
            this.razButton.Name = "razButton";
            this.razButton.Size = new System.Drawing.Size(132, 40);
            this.razButton.TabIndex = 2;
            this.razButton.Text = "RAZ";
            this.razButton.UseVisualStyleBackColor = true;
            this.razButton.Click += new System.EventHandler(this.razButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(145, 83);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 20);
            this.label1.TabIndex = 3;
            this.label1.Text = "Total";
            // 
            // labelValCompteur
            // 
            this.labelValCompteur.AutoSize = true;
            this.labelValCompteur.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelValCompteur.Location = new System.Drawing.Point(248, 83);
            this.labelValCompteur.Name = "labelValCompteur";
            this.labelValCompteur.Size = new System.Drawing.Size(0, 37);
            this.labelValCompteur.TabIndex = 4;
            this.labelValCompteur.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // title
            // 
            this.title.AutoSize = true;
            this.title.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.title.Location = new System.Drawing.Point(92, 2);
            this.title.Name = "title";
            this.title.Size = new System.Drawing.Size(266, 42);
            this.title.TabIndex = 5;
            this.title.Text = "Basic Counter";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(438, 206);
            this.Controls.Add(this.title);
            this.Controls.Add(this.labelValCompteur);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.razButton);
            this.Controls.Add(this.decButton);
            this.Controls.Add(this.incButton);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button incButton;
        private System.Windows.Forms.Button decButton;
        private System.Windows.Forms.Button razButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labelValCompteur;
        private System.Windows.Forms.Label title;
    }
}

